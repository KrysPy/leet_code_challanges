class TreeNode:
    def __init__(self, data) -> None:
        self.data = data
        self.left = None
        self.right = None
        
    def add_child(self, data):
        # unique values
        if self.data == data:
            return
        
        if data < self.data:
            if self.left:
                self.left.add_child(data)
            else:
                self.left = TreeNode(data)
        else:
            if self.right:
                self.right.add_child(data)
            else:
                self.right = TreeNode(data)
                

class Solution:
    def build_tree(self, preorder: list, inorder: list):
        root = TreeNode(preorder[0])
        

if __name__ == "__main__":
    elements = [17, 4, 1, 20, 9, 23, 18, 34]

