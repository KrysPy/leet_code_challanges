import math


class Solution:
    def __init__(self) -> None:
        pass
    
    def find_median_sorted_arrays(self, nums1: list[int], nums2: list[int]) -> float:
        merged_list = nums1 + nums2
        merged_list.sort()
        mid_point = len(merged_list) / 2
        
        if len(merged_list) % 2 != 0:
            return merged_list[math.floor(mid_point)]
        else:
            mid_sum = merged_list[mid_point] + merged_list[mid_point - 1]
            return round(mid_sum / 2, 2 )
    
    
if __name__=='__main__':
    solution = Solution()
    solution.find_median_sorted_arrays([1], [2])