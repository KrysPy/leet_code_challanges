class Solution:
    def length(self, s: str) -> int:
        substrings = set()
        itr = 0
        
        while itr < len(s):
            letters = {s[itr]: 1}
            substring = s[itr]
            for i in range(itr + 1, len(s)):
                if s[i] not in letters.keys():
                    substring += s[i]
                    letters[s[i]] = 1
                else: break
            substrings.add(substring)
            itr += 1
            
        length = len(list(substrings)[0])
        for item in substrings:
            if len(item) > length:
                length = len(item)
        return length
        
if __name__ == '__main__':
    solution = Solution()
    solution.length("abcabcbb")
    solution.length("pwwkew")
    solution.length("bbbbbb")

