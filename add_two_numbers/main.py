"""
https://leetcode.com/problems/add-two-numbers/
"""
class ListNode:
    def __init__(self, data=None, next=None) -> None:
        self.data = data
        self.next = next
        

class LinkedList:
    def __init__(self) -> None:
        self.head = None
        
    def insert_at_beginning(self, data):
        self.validate_data(data)
        node = ListNode(data, self.head)
        self.head = node
    
    def insert_at_end(self, data):
        self.validate_data(data)
        node = ListNode(data, None)
        
        if self.head is None:
            self.head = node
            return
        
        itr = self.head
        
        while itr.next:
            itr = itr.next
            
        itr.next = node
    
    def add_elements(self, elements):
        for element in elements:
            self.insert_at_beginning(int(element))
            
    def display(self):
        _lst = []
        itr = self.head

        while itr:
            _lst.append(int(itr.data))
            itr = itr.next
        print(_lst)
        
    def list_from_int(self, value: int):
        self.add_elements(str(value))
    
    def get_length(self):
        itr = self.head
        length = 0
        while itr:
            length += 1
            itr = itr.next
        return length

    @staticmethod
    def validate_data(value):
        if value < 0: 
            raise ValueError("Negative value given")
        
        if value > 9:
            raise ValueError("Number bigger than 9")
        
        
class Solution:
    def add_two_numbers(self, l1: LinkedList, l2: LinkedList):
        if not l1.get_length() or not l2.get_length():
            raise Exception("One of given list is empty")
        
        l3 = LinkedList()
        itr1 = l1.head
        itr2 = l2.head
        carry = 0
        digit = 0
        
        while itr1 or itr2 or carry != 0:
            itr1_val = itr1.data if itr1 else 0
            itr2_val = itr2.data if itr2 else 0
            
            tmp = itr1_val + itr2_val + carry
            if tmp >= 10:
                digit = (tmp % 10)
                carry = tmp // 10
            else:
                digit = tmp
                carry = 0
            itr1 = itr1.next if itr1 else None
            itr2 = itr2.next if itr2 else None
            l3.insert_at_end(digit)
        return l3
        

if __name__ == "__main__":
    l1 = LinkedList()
    l1.list_from_int(99)
    l2 = LinkedList()
    l2.list_from_int(1)
    solution = Solution()
    l3 = solution.add_two_numbers(l1, l2)
    l3.display()
